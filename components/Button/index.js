import React, { Component } from 'react'

import './style'

class Button extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isClicked: false,
      text: 'privet'
    }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick () {
    this.setState((state) => ({
      isClicked: !state.isClicked
    }))

    setTimeout(() => {
      this.setState((state) => ({
        isClicked: !state.isClicked
      }))
    }, 5000)
  }

  render () {
    const { text } = this.state
    return (
      <button
        className="button-main"
        onClick={this.handleClick}>{text}
      </button>
    )
  }
}

export default Button
