module.exports = {
    root: true,
    parserOptions: {
      parser: 'babel-eslint'
    },
    env: {
      browser: true,
    },
    extends: [
      'standard',
      'plugin:react/recommended'
    ],
    // add react plugin
    plugins: [
      'react'
    ],
    rules: {
      "no-unused-vars": ["error", { "vars": "all", "args": "after-used", "ignoreRestSiblings": false }],
      'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
    }
  }