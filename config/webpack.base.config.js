const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackMessages = require('webpack-messages');

const htmlPlugin = new HtmlWebpackPlugin({
  template: './template/index.html',
  filename: './index.html'
});

const textPlugin = new ExtractTextPlugin('style.css');

module.exports = {
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, '../dist'),
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.scss', '.css', '.json']
  },
  devtool: 'inline-source-map',
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader'
      }
    },
    // linting
    {
      test: /\.(js|jsx)$/,
      loader: 'eslint-loader',
      exclude: /node_modules/,
      options: {
        emitWarning: true
      }
    },
    {
      test: /\.(css|scss)$/,
      exclude: /node_modules/,
      use: ['css-hot-loader'].concat(
        ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            'css-loader',
            'sass-loader'
          ]
        })
      )
    },
    {
      test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
      loader: 'url-loader',
      options: {
        limit: 10000,
      }
    },
    {
      test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
      loader: 'url-loader',
      options: {
        limit: 10000,
      }
    },
    {
      test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
      loader: 'url-loader',
      options: {
        limit: 10000,
      }
    }
    ]
  },
  plugins: [
    htmlPlugin,
    textPlugin,
    new WebpackMessages({
      name: 'client',
      logger: str => console.log(`>> ${str}`)
    })
  ]
}