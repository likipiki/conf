import React, { Component } from 'react'
import './style'

import Button from '../../components/Button'

class App extends Component {
  render () {
    return (
      <section className="container">
        <Button />
      </section>
    )
  }
}

export default App
